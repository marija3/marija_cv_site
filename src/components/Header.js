import {useMemo} from "react";
import './Main.css';
import './Contact.css';

function Header() {
    const location = useMemo(() => {
        return window.location.href.replace("#", "");
    }, [window.location.href]);

    const logButtonHref = useMemo(() => {
        return location.includes("login") || location.includes("private-area")?
                location.replaceAll(/((private-area)|(login))(\/)*/g, "") :
                location + "login";
    }, [location]);

    const contactButtonHref = useMemo(() => {
        if(location.includes("contact")) {
            return location.replace("login", "");
        }
        return  location.replace("login", "") + "contact/"
    }, [location]);

    const aboutmeButtonHref = useMemo(() => {
        return  location.replaceAll(/((contact)|(login))(\/)*/g, "");
    }, [location]);


    return (
        <header className={"MainHeader"}>
            <div className={"MainHeaderContent"}>
                <div className={"TextLogo"}>
                    <div className="LogoSymbol">MZ</div>
                    <div className="LogoText"><span>Marija Zdolsek</span></div>
                </div>

                <div className={location.includes("contact") && !location.includes('login') ? "ContactSiteNav" : "MainSiteNav"}>
                    <ul className={location.includes("contact") && !location.includes('login')  ? "ContactSiteNavList" : "MainSiteNavList"}>
                        <li><a href={aboutmeButtonHref}>About me</a></li>
                        <li><a href={contactButtonHref}>Contact</a></li>
                        {!location.includes("login") && <li><a href={logButtonHref}>{location.includes("private-area") ? "Logout" : "Login"}</a></li>}
                    </ul>
                </div>
            </div>
        </header>
    );
}

export default Header;